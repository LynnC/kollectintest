package com.lynn.kollectintest.di

import com.lynn.kollectintest.data.api.PostService
import dagger.Module
import dagger.Provides

@Module
class ApiModule {

    @Provides
    fun providePostService(): PostService = PostService.create()
}