package com.lynn.kollectintest.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.paging.PagedList
import com.lynn.kollectintest.data.Post
import com.lynn.kollectintest.data.Repository
import com.lynn.kollectintest.data.model.GetPostResult

class PostListViewModel(private val repository: Repository) : ViewModel() {

    private val currentPageNoLiveData = MutableLiveData<Int>()
    private val getPostResult: LiveData<GetPostResult> =
        Transformations.map(currentPageNoLiveData) {
            repository.loadPost(it)
        }

    val posts: LiveData<PagedList<Post>> =
        Transformations.switchMap(getPostResult) { it -> it.data }
    val hasMore: LiveData<Boolean> = Transformations.switchMap(getPostResult) { it -> it.hasMore }
    val networkErrors: LiveData<String> = Transformations.switchMap(getPostResult) { it ->
        it.networkErrors
    }

    fun loadMorePost(page: Int) {
        currentPageNoLiveData.postValue(page)
    }

    fun listScrolled(
        totalItemCount: Int,
        visibleItemCount: Int,
        lastVisibleItems: IntArray
    ): Boolean {

        val lastVisibleItem = getLastPosition(lastVisibleItems) ?: return false

        if (visibleItemCount + lastVisibleItem + VISIBLE_THRESHOLD >= totalItemCount) {
            val currentPageNo = currentPage()
            if (currentPageNo != null) {
                repository.loadMore(currentPageNo)
            }
            return true
        }
        return false
    }

    private fun getLastPosition(positions: IntArray): Int? = positions.max()

    fun currentPage(): Int? = currentPageNoLiveData.value

    companion object {
        private const val VISIBLE_THRESHOLD = 5
    }
}