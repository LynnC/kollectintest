package com.lynn.kollectintest.ui.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.lynn.kollectintest.R
import com.lynn.kollectintest.data.Post
import java.text.DecimalFormat
import java.text.NumberFormat

class PostViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(R.layout.layout_post, parent, false)
) {

    private val imageViewPost = itemView.findViewById<ImageView>(R.id.imageViewPost)
    private val imageViewTag = itemView.findViewById<ImageView>(R.id.imageViewTag)

    private val textViewContent = itemView.findViewById<TextView>(R.id.textViewContent)

    private val imageViewAvatar = itemView.findViewById<ImageView>(R.id.imageViewAvatar)
    private val textViewUserName = itemView.findViewById<TextView>(R.id.textViewUserName)

    private val textViewLike = itemView.findViewById<TextView>(R.id.textViewLike)

    var post: Post? = null

    private val glidePostImageRequestManager = Glide.with(parent.context)
    private val glideAvatarImageRequestManager = Glide.with(parent.context)

    private val postImageRequestBuilder = glidePostImageRequestManager.asDrawable()
        .transition(DrawableTransitionOptions.withCrossFade())
        .apply(RequestOptions.fitCenterTransform())
        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)

    private val avatarImageRequestBuilder = glideAvatarImageRequestManager.asDrawable()
        .transition(DrawableTransitionOptions.withCrossFade())
        .apply(RequestOptions.circleCropTransform())
        .override(parent.resources.getDimensionPixelSize(R.dimen.avatar_size))
        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)

    fun bind(post: Post) {
        this.post = post

        postImageRequestBuilder.load(post.cover.url)
            .override(post.cover.width, post.cover.height)
            .into(imageViewPost)
        avatarImageRequestBuilder.load(post.user.coover)
            .into(imageViewAvatar)

        // TODO: When to show the tag icon?
        imageViewTag.visibility = if (post.status == Post.PUBLISHED) View.VISIBLE else View.GONE

        textViewContent.text = post.title

        textViewUserName.text = post.user.name

        textViewLike.text = formatter.format(post.likes)

        textViewLike.setCompoundDrawablesWithIntrinsicBounds(
            if (post.isLike) R.drawable.ic_heart_red else R.drawable.ic_heart,
            0,
            0,
            0
        )
    }

    fun cancelLoadingImage() {
        glidePostImageRequestManager.clear(imageViewPost)
        glideAvatarImageRequestManager.clear(imageViewAvatar)
    }

    companion object {
        val formatter: NumberFormat = DecimalFormat("#,###")

    }
}