package com.lynn.kollectintest.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.lynn.kollectintest.data.Repository
import com.lynn.kollectintest.ui.home.PostListViewModel

class ViewModelFactory(private val repository: Repository) :
    ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PostListViewModel::class.java)) {
            return PostListViewModel(repository) as T
        }

        throw IllegalArgumentException("Unknown ViewModelclass")
    }

}