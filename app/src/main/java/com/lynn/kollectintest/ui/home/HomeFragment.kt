package com.lynn.kollectintest.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.paging.PagedList
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.lynn.kollectintest.MyApplication
import com.lynn.kollectintest.R
import com.lynn.kollectintest.data.Post
import kotlinx.android.synthetic.main.fragment_home.view.*
import javax.inject.Inject

class HomeFragment : Fragment() {

    @Inject
    lateinit var viewModel: PostListViewModel
    private val postAdapter = PostAdapter()

    // TODO: Should add it back when adding refresh feature.
    private lateinit var onScrollListener: RecyclerView.OnScrollListener

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        MyApplication.appComponent!!.inject(this)
        val thread = Thread.currentThread()
        val root = inflater.inflate(R.layout.fragment_home, container, false)

        setupRecyclerView(root)
        initAdapter(root)

        val currentPageNo = savedInstanceState?.getInt(CURRENT_PAGE_NO) ?: 1
        viewModel.loadMorePost(currentPageNo)

        return root
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(CURRENT_PAGE_NO, viewModel.currentPage() ?: 1)
    }

    private fun initAdapter(root: View) {

        root.recyclerViewPost.adapter = postAdapter
        viewModel.posts.observe(this, Observer<PagedList<Post>> {
            postAdapter.submitList(it)
            root.progressBar.visibility = View.GONE
        })
        viewModel.hasMore.observe(
            this,
            Observer<Boolean> {
                if (onScrollListener != null) {
                    root.recyclerViewPost.removeOnScrollListener(onScrollListener)
                }
                root.progressBar.visibility = View.GONE
            })
        viewModel.networkErrors.observe(this, Observer<String> {
            Toast.makeText(context, "Network error: $it", Toast.LENGTH_LONG).show()
        })
    }

    private fun setupRecyclerView(root: View) {

        val staggeredGridLayoutManager =
            StaggeredGridLayoutManager(GRID_SPAN_COUNT, StaggeredGridLayoutManager.VERTICAL)

        root.recyclerViewPost.apply {
            layoutManager = staggeredGridLayoutManager
            adapter = postAdapter
        }

        onScrollListener = object : RecyclerView.OnScrollListener() {

            private val VISIBLE_ITEM_ARRAY = IntArray(GRID_SPAN_COUNT)

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                val totalItemCount = staggeredGridLayoutManager.itemCount
                val visibleItemCount = staggeredGridLayoutManager.childCount
                staggeredGridLayoutManager.findLastVisibleItemPositions(VISIBLE_ITEM_ARRAY)

                root.progressBar.visibility = if (viewModel.listScrolled(
                        totalItemCount,
                        visibleItemCount,
                        VISIBLE_ITEM_ARRAY
                    )
                ) View.VISIBLE else View.GONE
            }
        }

        root.recyclerViewPost.addOnScrollListener(onScrollListener)
    }

    companion object {
        private const val GRID_SPAN_COUNT = 2

        private const val CURRENT_PAGE_NO: String = "current_page_no"
    }
}