package com.lynn.kollectintest.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class User(
    @PrimaryKey
    @SerializedName("user_id")
    @ColumnInfo(name = "id")
    val uid: String,
    @ColumnInfo(name = "name")
    val name: String,
    @SerializedName("user_cover")
    @ColumnInfo(name = "cover")
    val coover: String
)