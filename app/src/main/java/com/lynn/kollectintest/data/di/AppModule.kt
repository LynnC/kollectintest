package com.lynn.kollectintest.data.di

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import com.lynn.kollectintest.data.Repository
import com.lynn.kollectintest.data.api.PostService
import com.lynn.kollectintest.data.db.LocalCache
import com.lynn.kollectintest.data.db.MyDatabase
import com.lynn.kollectintest.ui.ViewModelFactory
import com.lynn.kollectintest.ui.home.PostListViewModel
import dagger.Module
import dagger.Provides
import java.util.concurrent.Executors
import javax.inject.Singleton

@Module
class AppModule(private val context: Context) {

    @Provides
    @Singleton
    fun provideContext() = context

    @Provides
    fun provideCache() = LocalCache(
        MyDatabase.get(context).postDao(),
        Executors.newSingleThreadExecutor()
    )

    @Provides
    fun provideRepository(postService: PostService, cache: LocalCache): Repository {
        return Repository(postService, cache)
    }

    @Provides
    fun providePostListViewModel(viewModelFactory: ViewModelProvider.Factory): PostListViewModel =
        viewModelFactory.create(PostListViewModel::class.java)

    @Provides
    fun provideViewModelFactory(repository: Repository): ViewModelProvider.Factory =
        ViewModelFactory(repository)
}