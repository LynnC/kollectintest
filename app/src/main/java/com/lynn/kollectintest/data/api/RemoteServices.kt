package com.lynn.kollectintest.data.api

import android.util.Log
import com.lynn.kollectintest.data.Post
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


private const val TAG = "PostService"

fun loadPosts(
    service: PostService,
    sort: String,
    pageNo: Int = 1,
    limit: Int,
    onSuccess: (posts: List<Post>, hasMore: Boolean) -> Unit,
    onError: (errorMes: String) -> Unit
) {

    service.getPosts(sort, limit, pageNo).enqueue(
        object : Callback<GetPostResponse> {
            override fun onFailure(call: Call<GetPostResponse>?, t: Throwable) {
                Log.d(TAG, "fail to get data")
                onError(t.message ?: "unknown error")
            }

            override fun onResponse(
                call: Call<GetPostResponse>?,
                response: Response<GetPostResponse>
            ) {
                Log.d(TAG, "got a response $response")
                if (response.isSuccessful) {
                    val posts = response.body()?.datas ?: emptyList()
                    val hasMore = response.body()?.hasMore ?: false
                    onSuccess(posts, hasMore)
                } else {
                    onError(response.errorBody()?.string() ?: "Unknown error")
                }
            }
        }
    )
}