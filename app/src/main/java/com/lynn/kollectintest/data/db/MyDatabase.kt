package com.lynn.kollectintest.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.lynn.kollectintest.data.Post
import com.lynn.kollectintest.data.User
import com.lynn.kollectintest.data.db.dao.PostDao
import com.lynn.kollectintest.data.db.dao.UserDao

@Database(entities = [Post::class, User::class], version = 1)
abstract class MyDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao
    abstract fun postDao(): PostDao

    companion object {
        private var instance: MyDatabase? = null

        fun get(context: Context): MyDatabase {
            if (instance == null) {
                instance = Room.databaseBuilder(
                    context.applicationContext,
                    MyDatabase::class.java,
                    "MyDatabase"
                )
                    .build()
            }

            return instance!!
        }
    }
}