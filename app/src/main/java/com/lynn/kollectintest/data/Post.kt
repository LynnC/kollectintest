package com.lynn.kollectintest.data

import androidx.annotation.Nullable
import androidx.room.*
import com.google.gson.annotations.SerializedName

@Entity
data class Post(
    @PrimaryKey
    val id: String,
    val title: String,
    val likes: Long,
    @SerializedName("is_like")
    val isLike: Boolean,
    @Embedded(prefix = "user_")
    val user: User,
    @Embedded(prefix = "cover_")
    val cover: Cover,
    val status: Int
) {
    companion object {
        const val DRAFTS = 0
        const val PUBLISHED = 1
        const val UNVERIFIED = 2
        const val REJECTED = 3
    }
}
