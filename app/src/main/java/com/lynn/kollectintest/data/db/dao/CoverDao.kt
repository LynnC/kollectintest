package com.lynn.kollectintest.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.lynn.kollectintest.data.Cover
import com.lynn.kollectintest.data.User

@Dao
interface CoverDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCover(vararg covers: Cover)

    @Query("SELECT * FROM Cover WHERE url=:url")
    fun getCover(url: String): User
}