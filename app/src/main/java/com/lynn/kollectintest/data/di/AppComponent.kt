package com.lynn.kollectintest.data.di

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import com.lynn.kollectintest.data.Repository
import com.lynn.kollectintest.data.api.PostService
import com.lynn.kollectintest.data.db.LocalCache
import com.lynn.kollectintest.di.ApiModule
import com.lynn.kollectintest.ui.home.HomeFragment
import com.lynn.kollectintest.ui.home.PostListViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, ApiModule::class])
interface AppComponent {

    fun inject(homeFragment: HomeFragment)

    fun context(): Context
    fun cache(): LocalCache
    fun repository(): Repository
    fun postListViewModel(): PostListViewModel
    fun viewModelFactory(): ViewModelProvider.Factory
    fun postService(): PostService
}