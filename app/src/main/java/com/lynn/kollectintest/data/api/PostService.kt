package com.lynn.kollectintest.data.api

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*


open interface PostService {

    @Headers(
        "Accept: application/json",
        "x-api-version: 2"
    )
    @GET("/api/social/posts/")
    fun getPosts(
        @Query("sor") sort: String,
        @Query("limit") limit: Int,
        @Query("pageNo") pageNo: Int
    ): Call<GetPostResponse>

    companion object {

        private const val BASE_URL = "https://interview-api.kollectin.com"

        fun create(): PostService {
            val logger = HttpLoggingInterceptor()
            logger.level = HttpLoggingInterceptor.Level.BASIC

            val client = OkHttpClient.Builder()
                .addInterceptor(logger)
                .build()

            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(PostService::class.java)
        }
    }
}