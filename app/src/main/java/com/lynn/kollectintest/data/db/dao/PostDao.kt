package com.lynn.kollectintest.data.db.dao

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.lynn.kollectintest.data.Post

@Dao
interface PostDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPosts(vararg posts: Post)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPosts(posts: List<Post>)

    @Query("SELECT * FROM Post")
    fun loadPosts(): DataSource.Factory<Int, Post>

    @Query("DELETE FROM Post WHERE id=:postId")
    fun delete(postId: String)

}
