package com.lynn.kollectintest.data

import androidx.lifecycle.MutableLiveData
import com.lynn.kollectintest.data.api.PostService
import com.lynn.kollectintest.data.api.loadPosts
import com.lynn.kollectintest.data.db.LocalCache
import com.lynn.kollectintest.data.model.GetPostResult

class Repository(
    private val service: PostService,
    private val cache: LocalCache
) {

    private val hasMore = MutableLiveData<Boolean>()

    private val networkErrors = MutableLiveData<String>()

    // avoid triggering multiple requests in the same time
    private var isRequestInProgress = false


    /**
     * Load posts whose post time is before postTime.
     */
    fun loadPost(pageNo: Int): GetPostResult {
        requestAndSaveData(pageNo)

        // Get data from the local cache
        val data = cache.loadPosts()

        return GetPostResult(data, hasMore, networkErrors)
    }

    fun loadMore(pageNo: Int) {
        requestAndSaveData(pageNo + 1)
    }

    private fun requestAndSaveData(pageNo: Int) {
        if (isRequestInProgress) return

        isRequestInProgress = true

        loadPosts(service, "pop", pageNo, NETWORK_PAGE_SIZE,
            { posts, hasMore ->
                cache.insertPost(posts) {
                    isRequestInProgress = false
                }
            }, { error ->
                networkErrors.postValue(error)
                isRequestInProgress = false
            }
        )

        isRequestInProgress = false
    }

    companion object {
        const val NETWORK_PAGE_SIZE = 10
    }
}