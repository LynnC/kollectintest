package com.lynn.kollectintest.data.model

import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import com.lynn.kollectintest.data.Post

data class GetPostResult(
    val data: LiveData<PagedList<Post>>,
    val hasMore: LiveData<Boolean>,
    val networkErrors: LiveData<String>
)
