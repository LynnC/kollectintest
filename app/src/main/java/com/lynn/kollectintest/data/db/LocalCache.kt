package com.lynn.kollectintest.data.db

import androidx.lifecycle.LiveData
import androidx.paging.Config
import androidx.paging.PagedList
import androidx.paging.toLiveData
import com.lynn.kollectintest.data.Post
import com.lynn.kollectintest.data.db.dao.PostDao
import java.util.concurrent.Executor

class LocalCache(private val postDao: PostDao, private val ioExecutor: Executor) {

    fun insertPost(posts: List<Post>, insertFinished: () -> Unit) {
        ioExecutor.execute {
            postDao.insertPosts(posts)
            insertFinished()
        }
    }

    fun loadPosts(): LiveData<PagedList<Post>> =
        postDao.loadPosts()
            .toLiveData(
                Config(
                    pageSize = 10,
                    enablePlaceholders = true,
                    maxSize = 200
                )
            )
}