package com.lynn.kollectintest.data.api

import com.google.gson.annotations.SerializedName
import com.lynn.kollectintest.data.Post

data class GetPostResponse(
    val datas: List<Post> = emptyList(),
    val total: Int,
    @SerializedName("has_more") val hasMore: Boolean,
    @SerializedName("current_page") val currentPage: Int
)