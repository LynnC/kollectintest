package com.lynn.kollectintest

import android.app.Application
import com.lynn.kollectintest.data.di.AppComponent
import com.lynn.kollectintest.data.di.AppModule
import com.lynn.kollectintest.data.di.DaggerAppComponent
import com.lynn.kollectintest.di.ApiModule

class MyApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .apiModule(ApiModule())
            .build()
    }

    companion object {

        var appComponent: AppComponent? = null
    }
}