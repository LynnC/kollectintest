package com.lynn.kollectintest.di

import com.lynn.kollectintest.data.api.PostService
import com.lynn.kollectintest.data.api.createMockPostService
import dagger.Module
import dagger.Provides

@Module
class ApiModule {

    @Provides
    fun providePostService():PostService = createMockPostService()
}