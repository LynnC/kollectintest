package com.lynn.kollectintest.data.api

import com.lynn.kollectintest.data.Cover
import com.lynn.kollectintest.data.Post
import com.lynn.kollectintest.data.User
import okhttp3.Request
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


fun createMockPostService(): PostService {

    var page = 0
    return object : PostService {
        override fun getPosts(pageNo: Int, limit: Int): Call<GetPostResponse> {

            return object : Call<GetPostResponse> {
                override fun enqueue(callback: Callback<GetPostResponse>) {

                    callback.onResponse(
                        null,
                        Response.success(200, GetPostResponse(getFakePosts(), 30, true, page++))
                    )
                }

                override fun isExecuted(): Boolean {
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }

                override fun clone(): Call<GetPostResponse> {
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }

                override fun isCanceled(): Boolean {
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }

                override fun cancel() {
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }

                override fun execute(): Response<GetPostResponse> {
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }

                override fun request(): Request {
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }

            }
        }
    }
}


fun getFakePosts(): List<Post> {

    val fakeUser = User(
        "3000",
        "Nike",
        "https://s3-alpha-sig.figma.com/img/1ee6/abe6/f22d2f71f7c419eb367a3cf86905487b?Expires=1584921600&Signature=fC60V2-etqJ6QqBqqFhFb4RABL6ctyTLuyKthgvRV7g7gVNjH7PN33D7KTUfVqeWgIwvn4hqncJfPGSJkAXx3j0zeMwvudVcc-GRjfqfO02FhM~FRCemWBpdPBMXokeX5PpiR90p2yJqyVLCln9JTXr0YXLmxb72Lo5v9-XVbMm2LHQVbesINMe0BEcT-qRGSAZDNHJPg~xpQOIfgCqDOiw7ZDne9AnHUtvGM1TJQ0BUXae0Lf~l1vedO2aQw9eOAidkx~Z2pYc~JrLgJMrVcy60avZ0DwkWmFmXWCzaOn-1yBPc~uaBoorVMna~dejM~R7kukaRWBDM9IaOj~3ulg__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA"
    )

    val postImages = arrayOf(
        "https://s3-alpha-sig.figma.com/img/aae6/47e0/184c6301ceb5275d31d7e5ce751c4dbb?Expires=1584921600&Signature=cBSh-DO6yrONVKjdpyHdIN1APhvnvDRniZrYKMLkQwlOrt1OlbTv10xBA-0GNbqS1-cJUx9d31iVGByRWVLGNvvtGqKzjgK2~KG1csrwztgT51JS9zXhkwhwGRtMUljX1QpYlsN2FHbAVV~V5fGQN53tipv1GZDrtwxL~7GgX2CY5zoUZLC5tSzoOQjx7g3~yGyQxV~osRuXMUejXym~vl8DY4zuPmFv1mfePECNnmNrMFOHO544UGJSQO~Le6Ha5UxkmmDECTJDpW5OiYi2rQt3GUDwM6l0I9nz~QM1MLjQzdCV74Hd5qzZOC07xJ-hUQOHU6lJsibro5QiqGvk-g__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA",
        "https://s3-alpha-sig.figma.com/img/57b0/2441/a2ae738045a0bd26a2a5bca554400490?Expires=1584921600&Signature=Fyk4oDLtNfRJNeB9TV9x-08-xSVfs4~z271uWkwqadjFk-UQgKlAWJ9Oj8NZM1X7DjVK2MEGqATkBaN8fHVcan6WU~JhQnDoHIW18tht64pwLtr4m16~qYpGYyI0bP9zinG860Zkm77xnk~FAXpAg34gzIsewA979nT4U-~CxcmPqvdu6TtQI05TnDoDjNIZN4kKujuo6XvzSL9ChrXd8~ymNmMLpxm60vmKnR4QTNjm8eDHc5ew9~nqYskYVxJEm09igkvXDmKbdOCoiN1EicoiepsRkGcqW3lQ0sVTd5N3QRxgsVOdoCg-iJN7o6QLFPVxQRQ4CJI825i10cvF0g__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA",
        "https://s3-alpha-sig.figma.com/img/5948/41d5/ad9fb476c640b787cd4ae5a3e9a515f3?Expires=1584921600&Signature=f6jAEypl9AL9dN-5zm0tyDqxGXhJtPvUgpGTOB8SnTRdH3hMvZvQ9E2M~2SimA9vuA2vrA-tsFw1mHpJVn-~7q6QSLXoiM1PqbB3ZUm6fl9fwifNuIMbPQw25oQmq~A-ls1IHNk2GlCKNPGn7Bd9stTBtXd4pCj~3ycQFvH7Rxu2hNHVTStcv8pLZvK3BY0WLukVEG~pau4kli-gWiD63N-FrxVyuFNm-WPG6Up1Z0FDnmoQyEv8D2UBpJXBAj-j8X1PExHQnf~G0HUXBwZLpJdZ0OUYZZc~BJELwdxnrLScvP4CSJKxEb0w~sS6qRVTg5IUkXof8XbMIgt0lZa-Fg__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA",
        "https://s3-alpha-sig.figma.com/img/d220/d60c/ee8e954fc70c6e2aed6c464f2e93a9e9?Expires=1584921600&Signature=Cw89QoZmmPPTHDt~dObzUimTFy5M6c9sokI0LfWK~tIIIRhqN86tXhf53GsSoHz8ogdFyQTcxptlwi1uEBhlQwz~m5oQBHkEc8v9Uu2bAI7mC80~tLfOMAXrO64dXolUVI7RCNp0WYhKvtdIDxFep98XCj0uXK3UK9CWkW0wkTJoEBiFE7YfsGXl6-UJ24Eqo~Ih3z0ILURP1KRtbIfnX22GLeJwsyEwFCdVbDxWYlvMkHxlukWZ9ebdwuMf4xEZK6urVeelOf7lECoSfDd-k-vTkBjpK4mcFK~W9rmxD2qgfTJaEiKHmoBPxo4e8yXRgFinjN9GwRhlsPUVLrS2ow__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA"
    )

    val fakeTime = System.currentTimeMillis()

    val fakePosts = ArrayList<Post>()
    for (i in 0..20) {

        fakePosts.add(
            Post(
                "$fakeTime $i",
                "$i This is regular post with text and image.",
                1020,
                false,
                fakeUser,
                Cover(postImages[i % postImages.size], 100, 120),
                fakeUser.cover,
                i % postImages.size == 0
            )
        )

    }
    return fakePosts;
}
