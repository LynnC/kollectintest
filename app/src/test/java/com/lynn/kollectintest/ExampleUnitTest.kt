package com.lynn.kollectintest

import org.junit.Assert.assertEquals
import org.junit.Test
import java.text.DecimalFormat
import java.text.NumberFormat

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {

        val formatter: NumberFormat = DecimalFormat("#,###")
        assertEquals("1,000", formatter.format(1000))
        assertEquals("1,222,000", formatter.format(1222000))
        assertEquals("1,222,333,000", formatter.format(1222333000))
        assertEquals("100", formatter.format(100))
        assertEquals(4, 2 + 2)
    }
}
